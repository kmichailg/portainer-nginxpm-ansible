### Server Compatibility:
1. Linux, 64-bit
    - CentOS 7
    - Ubuntu LTS
    - Debian 10 (Buster)
    - Raspberry Pi OS / Raspbian (based on Debian Buster)

2. VPS with at least 2GB RAM, fresh install

***

### Installation
Log in to SSH as root and issue the command below:

#### Install on CentOS 7:
```
bash <(curl -s https://gitlab.com/kmichailg/portainer-nginxpm-ansible/-/raw/master/install_centos7.sh)
```

#### Install on Ubuntu LTS:
```
bash <(curl -s https://gitlab.com/kmichailg/portainer-nginxpm-ansible/-/raw/master/install_ubuntu.sh)
```

#### Install on Debian 10 (Buster) / Raspbian:
```
apt update && apt install -y curl
bash <(curl -s https://gitlab.com/kmichailg/portainer-nginxpm-ansible/-/raw/master/install_debian.sh)
```

***